#### Chat with Documents / RAG

1: Include document to chat history and include chat history in LLM context via the following settings:

![Document settings](../../docs/assets/ui/documents_ui.png)

2: Set Collections to be MyData

![Collections settings](../../docs/assets/ui/collections_ui.png)

3: Upload pdf (upload button next to submit button)

![Upload](../../docs/assets/ui/upload_docs.png)

This should make it so that any documents you upload are included as context to queries you make.

Below is an example output with monopoly pdf as source. 

![Example](../../docs/assets/ui/documents_output.png)

