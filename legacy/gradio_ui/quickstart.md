#### Interacting with the UI

![Control buttons](../../docs/assets/ui/control_buttons.png)

1: To ask a question, type your query in the input box and click the "Submit" button.

2: To save chat, click on "Save". 

Saved chats will appear in the top left.

![Saved chats](../../docs/assets/ui/chats_ui.png)

3: If you want to clear a chat (so you can start a blank conversation), first save it if you would like to keep it for later. Then click on "Clear", next to the "Save" button.