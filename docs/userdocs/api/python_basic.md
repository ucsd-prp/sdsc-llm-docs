#### Python Access

Replace <api_key\> with the actual key.

```python
import requests
import json

def test_model_list(api_key):
    response = requests.get("https://sdsc-llm-openwebui.nrp-nautilus.io/api/models",
                            headers={"Authorization": f"Bearer {api_key}"})

    return response.json()


def test_completions_model(api_key, model: str, messages):
    data = {
        "model": model,
        "messages": messages,
        "stream": False,
    }

    response = requests.post("https://sdsc-llm-openwebui.nrp-nautilus.io/api/chat/completions",
                             headers={"Authorization": f"Bearer {api_key}", "Content-Type": "application/json"},
                             data=json.dumps(data))

    return response.json()


if __name__ == "__main__":
    api_key = "<api_key>"

    models = test_model_list(api_key)
    print("Available Models:")
    print("-" * 20)
    for model in models['data']:
        print(model['id'])

    messages = [
        {
            "role": "system",
            "content": "You are a helpful assistant.",
        },
        {
            "role": "user",
            "content": "Solve 1 + 1",
        },
    ]

    llama3_test_completion = test_completions_model(api_key, "llama3-sdsc", messages)
    gemma2_test_completion = test_completions_model(api_key, "gemma2", messages)

    print("-" * 20)
    print("llama3-sdsc: " + llama3_test_completion["choices"][0]["message"]["content"].strip())
    print("-" * 20)
    print("gemma2: " + gemma2_test_completion["choices"][0]["message"]["content"].strip())
```
