### Setting System Prompt

Another cool feature of the SDSC LLM is that we can set a system prompt which will make sure that the LLM model act in a particular way favourable to the user.

In order to set the system prompt, the user needs to click on the top right corner, as shown by the arrow in the image below, to open chat controls.

Here we are gonna ask the model about what is nautilis. As it can be seen, the model gives a variety of response.

![Normal Response](../../assets/web_ui/normal_response_2.png)

Next we are gonna set a system prompt such that it gives only tech related answers.

![Controlled Response](../../assets/web_ui/controlled_response.png)

Here, it can be seen that the responses are now specific as per the set system prompt.
One common use of this feature is to make sure that the model works as a helper assistant like various other chat based LLM models already existing in the market, while providing extra flexibility for the user.
