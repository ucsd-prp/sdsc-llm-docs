### Adjusting Model Parameters

The chat control shown in the previous section has many other parameter which the user can tweak to get the results as per the requirement. All the user has to do is to click the same chat controls as mentioned in the previous section, and set the required model parameter such as temperature etc. as per user's convenience.

![chat controls](../../assets/web_ui/chat_controls.png)
