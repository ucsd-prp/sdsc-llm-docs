<!--
#### Testing Access

Replace <api_key\> with the actual key.

From the terminal, try:

```shell
curl https://sdsc-llm-api.nrp-nautilus.io/models \
  -H "Authorization: Bearer <api_key>"

printf "\n\n------------------------------------\n\n"

curl https://sdsc-llm-api.nrp-nautilus.io/chat/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer <api_key>" \
  -d '{
    "model": "meta-llama/Meta-Llama-3.1-70B-Instruct",
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "Hello!"
      }
    ]
  }'

printf "\n\n------------------------------------\n\n"

curl https://sdsc-llm-api.nrp-nautilus.io/chat/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer <api_key>" \
  -d '{
    "model": "mistralai/Mixtral-8x7B-Instruct-v0.1",
    "messages": [
      {
        "role": "user",
        "content": "Hello!"
      }
    ]
  }'
```
-->

#### Testing Access

Replace <api_key\> with the actual key.

From the terminal, try:

```shell
curl -X GET "https://sdsc-llm-openwebui.nrp-nautilus.io/api/models" \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer <api_key>' | jq '.data[].id'

```

The above will get the list of models present, note: jq is used to extract only id form the response json. (If jq is not present, it can be installed using "sudo apt install jq")

```shell
curl -X POST "https://sdsc-llm-openwebui.nrp-nautilus.io/api/chat/completions" \
-H "Authorization: Bearer <api_key>" \
-H "Content-Type: application/json" \
-d '{
  "model": "llama3-sdsc",
  "messages": [
    {
        "role": "system",
        "content": "You are a helpful assistant."
    },
    {
      "role": "user",
      "content": "What does kubectl command do?"
    }
  ],
  "stream": false
}'

printf "\n\n------------------------------------\n\n"

curl -X POST "https://sdsc-llm-openwebui.nrp-nautilus.io/api/chat/completions" \
-H "Authorization: Bearer <api_key>" \
-H "Content-Type: application/json" \
-d '{
    "model": "gemma2",
    "messages": [
      {
        "role": "user",
        "content": "Hello!"
      }
    ]
 }'
```
