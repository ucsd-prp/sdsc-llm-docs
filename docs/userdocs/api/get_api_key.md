# Generating API Key

User's personal API key can be generated from the [ChatUI](https://sdsc-llm-openwebui.nrp-nautilus.io/) itself.

After logging in the ChatUI using CILogon, follow the below steps to generate the API Key.

Step 1: Click on the top right corner profile picture, then click on the settings.

![Setting](../../assets/api/Setting.png)

Step 2: A setting menu will pop up, locate Account setting and click on the same.

![Account](../../assets/api/account.png)

Step 3: Click on show API keys.

![APIKey](../../assets/api/api_key.png)

Step 4: Either generate a new API key or view the existing one in case one already exists.

![APIKey_view](../../assets/api/api_key_place.png)

Great!! you created your API key.
