# API Overview

Our base URL is at: [https://sdsc-llm-openwebui.nrp-nautilus.io/](https://sdsc-llm-openwebui.nrp-nautilus.io/)

### Models Endpoint - [https://sdsc-llm-openwebui.nrp-nautilus.io/api/models](https://sdsc-llm-openwebui.nrp-nautilus.io/api/models)

List the current available models.

### Chat Completions Endpoint - [https://sdsc-llm-openwebui.nrp-nautilus.io/api/chat/completions](https://sdsc-llm-openwebui.nrp-nautilus.io/api/chat/completions)

Given a list of messages comprising a conversation, the model will return a response.
