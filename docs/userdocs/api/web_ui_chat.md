### Downloading Chat

Users can even download the specific chats by following some simple steps.

Just click on the 3 dots on the top right hand side beside chat controls and then click on the download option in the drop down menu, as shown by the red arrow in the image.

![Download](../../assets/web_ui/download_chat.png)

There are also various file extensions available for the user to download the chats in. Just click on the prefered file extension and a file containing chats will be downloaded.

![Download Options](../../assets/web_ui/download_options.png)
