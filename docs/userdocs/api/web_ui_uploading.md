### Uploading Documents

The SDSC LLM comes with the feature of uploading you own documents. Refer to the image below, click on the '+' button located on the left hand side of the chat bar as shown by the red arrow in the image.

![Web UI Image](../../assets/web_ui/file_upload.png)

Doing so an upload files button will pop up.

![Upload Image](../../assets/web_ui/upload.png)

Select the file you want to upload and carry on prompting. Refer to the image below, the LLM comes back with a response based on the document you provided.

![Upload Response](../../assets/web_ui/file_upload_response.png)
![Upload Response](../../assets/web_ui/file_upload_response.png)
