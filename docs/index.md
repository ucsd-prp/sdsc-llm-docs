## SDSC LLM

SDSC LLM is an LLM service that provides quick and easy access to open-source LLMs for the SDSC community.

The San Diego Supercomputer Center (SDSC) is part of the University of California San Diego (UCSD).

## UI and API Access Links

[Chat UI](https://sdsc-llm-openwebui.nrp-nautilus.io/): To access the LLMs, please log in with CILogin using your UCSD Active Directory credentials.

[API URL](https://sdsc-llm-docs.nrp.ai/userdocs/api/curl_basic/): To communicate with LLMs programatically (through an API or 'curl' comand), you will need an API key. To generate an API key kindly follow the steps under [generating api key](https://sdsc-llm-docs.nrp.ai/userdocs/api/get_api_key/) page.
Please do not share your personal API key with anyone else.

## LLMs License Information

Prior to using SDSC LLM, please read the license information for different models from the vendor sites listed below.

Please check the Meta website for [Llama3 license](https://llama.meta.com/llama3/license/) details. Additional information can be found here: [Llama Troubleshooting & FAQ](https://llama.meta.com/faq/).

Please check the [Mistral AI](https://mistral.ai/news/mistral-ai-non-production-license-mnpl/) site for non-production license information.

<!--Details of [Google's Gemma license](https://github.com/google-deepmind/gemma/blob/main/LICENSE) are available on github site. -->

## Technical Support

Technical support is available via [Matrix](https://docs.nrp.ai/userdocs/start/contact/). Join the room through the web hosted version of element.io as explained on this [Link](https://nationalresearchplatform.org/updates/matrix-chat-for-nautilus-users/). Please create an account and follow the instructions.

You can get support regarding the UI or API in the `SDSC-llm-support` Matrix room.

## Citing SDSC LLM

If SDSC LLM contributes to your work, please cite it using the following Digital Object Identifier (DOI):  [https://doi.org/10.71469/B14W2B](https://doi.org/10.71469/B14W2B)
  
Here is an example:

<font face="Courier">San Diego Supercomputer Center (2025): SDSC LLM. San Diego Supercomputer Center. Service. https://doi.org/10.71469/B14W2B</font>


## Use Cases
Examples of how SDSC LLM is used can be found [here](https://sdsc-llm-docs.nrp.ai/userdocs/use_cases/).

## Funding

Funding for SDSC LLM is provided by SDSC.

## Feedback

To provide feedback on SDSC LLM, please send email to sdsc-llm-feedback@ucsd.edu.
